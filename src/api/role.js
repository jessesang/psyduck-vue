import request from '@/scripts/request'

export function getRoles() {
  return request({
    url: 'role/getRoles',
    method: 'get'
  })
}

export function fetchList(params) {
  return request({
    url: 'role/list',
    method: 'get',
    params
  })
}

export function saveData(params) {
  return request({
    url: 'role/save',
    method: 'post',
    params
  })
}

export function removeData(id) {
  return request({
    url: 'role/delete',
    method: 'post',
    params: {
      id: id
    }
  })
}
