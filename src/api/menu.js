import request from '@/scripts/request'

export function getMenuTree() {
  return request({
    url: 'menu/getMenuTree',
    method: 'get'
  })
}

export function getMenusByRoleId(roleId) {
  return request({
    url: 'menu/getMenusByRoleId',
    method: 'get',
    params: {
      roleId
    }
  })
}

export function saveData(data) {
  return request({
    url: 'menu/save',
    method: 'post',
    data
  })
}

export function removeData(id) {
  return request({
    url: 'menu/delete',
    method: 'post',
    params: {
      id: id
    }
  })
}

export function getCurrentUserMenu() {
  return request({
    url: 'menu/getCurrentUserMenu',
    method: 'post'
  })
}

export function changeIsShow(id, isShow) {
  return request({
    url: 'menu/changeIsShow',
    method: 'post',
    params: {
      id: id,
      isShow: isShow
    }
  })
}

export function getMenuConfig(id) {
  return request({
    url: 'menu/getMenuConfig',
    method: 'post',
    params: {
      id
    }
  })
}
