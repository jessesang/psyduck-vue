import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import '@/styles/index.scss' // global css
import '@/styles/common.css'

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control
import './scripts/error-log' // error log

import elDragDialog from '@/directive/el-drag-dialog' // base on element-ui
Vue.use(elDragDialog)

import common from '@/scripts/common'
Vue.prototype.$common = common

import request from '@/scripts/request'
Vue.prototype.$request = request
Vue.prototype.$post = (url, data) => request.post(url, data, {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  transformRequest: [data => Object.keys(data).map(it => encodeURIComponent(it) + '=' + encodeURIComponent(data[it] === null || data[it] === undefined ? '' : data[it])).join('&')]
})
Vue.prototype.$postJson = (url, data) => request.post(url, JSON.stringify(data), {
  headers: {
    'Content-Type': 'application/json'
  }
})
Vue.prototype.$get = (url, data) => request({ url, params: data })

import hasPermission from '@/scripts/hasPermission'
Vue.use(hasPermission)

import PdTable from '@/components/Psyduck/pd-table'
import PdDialog from '@/components/Psyduck/pd-dialog'
import PdSelect from '@/components/Psyduck/pd-select'
import PdButton from '@/components/Psyduck/pd-button'
import PdUploadImage from '@/components/Psyduck/pd-upload-image'
import PdUploadFile from '@/components/Psyduck/pd-upload-file'
import PdUeditor from '@/components/Psyduck/pd-ueditor'
Vue.component('pd-table', PdTable)
Vue.component('pd-dialog', PdDialog)
Vue.component('pd-select', PdSelect)
Vue.component('pd-button', PdButton)
Vue.component('pd-upload-image', PdUploadImage)
Vue.component('pd-upload-file', PdUploadFile)
Vue.component('pd-ueditor', PdUeditor)

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI, {
  size: 'small'
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

document.body.addEventListener('keydown', e => {
  if (e.altKey && e.ctrlKey && e.shiftKey && e.key === 'A') {
    router.push('/magic/magic-api')
  }
})
